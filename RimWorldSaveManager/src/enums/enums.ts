﻿export class NameTypeValue {
    public static SINGLE = "NameSingle";
    public static TRIPLE = "NameTriple";
    public static NULL = "Null";
}

export class Gender {
    public static MALE = "Male";
    public static FEMALE = "Female"
}

export class BodyType {
    public static MALE = "Male";
    public static FEMALE = "Female";
    public static AVERAGE = "Average";
    public static THIN = "Thin";
    public static HULK = "Hulk";
    public static FAT = "Fat";
}

export class HeadType {
    public static AVERAGE = "Average"; 
    public static NARROW = "Narrow"; 
    public static NORMAL = "Normal"; 
    public static POINTY = "Pointy"; 
    public static WIDE = "Wide";
}

 export class BackstoryType {
    public static ADULTHOOD = "Adulthood";
    public static CHILDHOOD = "Childhood";
 }
