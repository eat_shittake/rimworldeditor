import Backstory from "../models/Backstory";
import * as cheerio from "cheerio";
import BackstoryTranslation from "../models/BackstoryTranslation";
import * as _ from "lodash";

export class ModelFactory {

    private backstoryCheerioDictionary: { [key: string]: Function }
    constructor() {
        this.backstoryCheerioDictionary = {};
        this.backstoryCheerioDictionary["Title"] = this.GetNodeText;
        this.backstoryCheerioDictionary["TitleShort"] = this.GetNodeText;
        this.backstoryCheerioDictionary["titleShort"] = this.GetNodeText;
        this.backstoryCheerioDictionary["BaseDesc"] = this.GetNodeText;
        this.backstoryCheerioDictionary["Slot"] = this.GetNodeText;
        this.backstoryCheerioDictionary["WorkDisables"] = this.ExtractList
        this.backstoryCheerioDictionary["SpawnCategories"] = this.ExtractList
        this.backstoryCheerioDictionary["SkillGains"] = this.ExtractSkillGains
    }

    private GetCheerioStatic(xml: string): CheerioStatic;
    private GetCheerioStatic(xml: CheerioElement): CheerioStatic;
    private GetCheerioStatic(xml: any): CheerioStatic {
        return cheerio.load(xml,{ xmlMode: true});
    }

    private GetCheerioElements(xml: string, selector: string): Cheerio ;
    private GetCheerioElements(xml: CheerioElement, selector: string): Cheerio ;
    private GetCheerioElements(xml: any, selector: string): Cheerio {
        return this.GetCheerioStatic(xml)(selector);
    }

    public ExtractBackStories(dtos: Cheerio): Array<Backstory> {
        let backstories = dtos.children();
        let models: Array<Backstory> = []
        for(let i = 0; i < backstories.length; i++){
            let model = new Backstory();
            let backstory = backstories[i];
            for(let j = 0; j < backstory.children.length; j++){
                let node = backstory.children[j];
                if(this.backstoryCheerioDictionary[_.upperFirst(node.name)]){
                    model[node.name] = this.backstoryCheerioDictionary[node.name](node);
                    models.push(model);
                }
            }
        }
        return models;
    }

    private GetNodeText(element: CheerioElement): string{
        return element.data;
    }

    private ExtractList(dto: CheerioElement): Array<string> {
        let list = [];

        if(!dto.children.length){
            return list;
        }

        dto.children.forEach((elem)=>{
            list.push(elem.children[0].data)
        });
        return list;
    }

    private ExtractSkillGains(dto: CheerioElement): { [name: string]: number } {
        var gains = {};
        if (!dto.children.length) {
            return gains;
        }

        for(let i = 0; i < dto.children.length; i++){
            let node = dto.children[i];
            gains[node.children[1].data] = +node.children[3].nodeValue
        }

        return gains;
    }

    public ExtractBackstoryTranslations(dtos: CheerioElement[]): BackstoryTranslation[]{
        return dtos.map(elem =>{
            let trans = new BackstoryTranslation();
            trans.TagName = _.upperFirst(elem.tagName);
            elem.children.forEach(node => {
                if(trans[_.upperFirst(node.name)]) {
                    trans[_.upperFirst(node.name)] = this.backstoryCheerioDictionary[_.upperFirst(node.data)](node);
                }
            }) 
                return trans;
        });
    }

    public ExtractBackstoryFromBio(tagName: string, dtos: Cheerio): Backstory[]{
        let backstories: Backstory[] = [];
        dtos.children().each((i, elem) => {
            let backstory = new Backstory();
            backstory.Slot = tagName
            if(elem.name && this.backstoryCheerioDictionary[elem.name]) {
                backstory[elem.name] = elem.data;
            }
        });
        return backstories;
    }

}

export var modelFactory = new ModelFactory();