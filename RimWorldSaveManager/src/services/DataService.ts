﻿import Race from "../models/Race";
import Pawn from "../models/Pawn";
import TraitDef from "../models/TraitDef";
import Hediff from "../models/Hediff";
import WorkType from "../models/WorkType";
import Faction from "../models/Faction";
import PawnRelationDef from "../models/PawnRelationDef";
import CrownType from "../models/CrownType";
import * as cheerio from "cheerio";
import { Gender, BodyType, HeadType } from "../enums/enums";
import { ApiService } from "./ApiService"
import Backstory from "../models/Backstory";
import { modelFactory } from "./ModelFactory";
import { Logger } from "./Logger";
import { resourceLoader } from "./ResourceLoader";

export default class DataService {

    private SaveDocument: any;
    private Animals: Array<Pawn>;
    //private TextInfo textInfo = CultureInfo.InvariantCulture.TextInfo;

    public static Traits: { [key: string]: TraitDef };
    public static Hediffs: { [key: string]: Hediff };
    public static HumanBodyPartDescription: { [key: string]: string };
    public static WorkTypes: Array<WorkType>;
    public static Genders: Array<string>;
    public static Factions: { [key: string]: Faction };
    public static PlayerFaction: Faction;
    public static PawnsByFactions: { [key: Faction]: Array<Pawn> };
    public static PawnsById: { [key: string]: Pawn };

    public static RaceDictionary: { [key: string]: Race };
    public static PawnRelationDefs: Array<PawnRelationDef>;
    public static CurrentGameTick: number;

    private pathsForLaodingData: { [key: string]: Array<string> };


    constructor() {
        DataService.Traits = {};
        DataService.Hediffs = {};
        DataService.HumanBodyPartDescription = {};
        DataService.WorkTypes = [];
        DataService.Genders = [];
        DataService.Factions = {};
        DataService.PawnsByFactions = {};
        DataService.PawnsById = {};
        DataService.RaceDictionary = {};
        DataService.PawnRelationDefs = [];
        this.pathsForLaodingData = {};

        const human = new Race();
        human.defName = Race.HUMAN;
        human.label = Race.HUMAN;
        DataService.Genders.push(Gender.FEMALE);
        DataService.Genders.push(Gender.MALE);
        human.bodyType.push(BodyType.MALE);
        human.bodyType.push(BodyType.FEMALE);
        human.bodyType.push(BodyType.AVERAGE);
        human.bodyType.push(BodyType.THIN);
        human.bodyType.push(BodyType.HULK);
        human.bodyType.push(BodyType.FAT);
        human.hairsByGender[Gender.FEMALE] = [];
        human.hairsByGender[Gender.MALE] = [];

        let array1 = [HeadType.AVERAGE, HeadType.NARROW];
        for (var firstType in array1) {
            if (array1.hasOwnProperty(firstType)) {
                const array = [HeadType.NORMAL, HeadType.POINTY, HeadType.WIDE];
                for (var secondType in array) {
                    if (array.hasOwnProperty(secondType)) {
                        human.headType.push(new CrownType(firstType, secondType));
                    }
                }
            }
        }

        DataService.RaceDictionary[human.defName] = human;



        resourceLoader.Load();

        // Gonna use:
        // <input type="file" webkitdirectory>
        // https://stackoverflow.com/questions/13624450/how-to-get-the-contents-of-the-directory-from-local-pc-in-javascript
        // For this to work, this is gonna be a chrome only app.

        var allXmlFiles = Directory.GetFiles("Mods", "*.xml", SearchOption.AllDirectories);

        foreach(var xmlFile in allXmlFiles)
        {
            string[] pathComponents = xmlFile.Split('\\');
            if (!pathComponents[2].Equals("Defs")) {
                continue;
            }
            string fileName = pathComponents[pathComponents.Length - 1];

            if (fileName.ToLower().Contains("race")) {
                List < string > pathList;
                if (!pathsForLaodingData.TryGetValue("race", out pathList)) {
                    pathList = new List<string>();
                    pathsForLaodingData["race"] = pathList;
                }
                pathList.Add(xmlFile);
            }
            if (fileName.ToLower().Contains("trait")) {
                List < string > pathList;
                if (!pathsForLaodingData.TryGetValue("trait", out pathList)) {
                    pathList = new List<string>();
                    pathsForLaodingData["trait"] = pathList;
                }
                pathList.Add(xmlFile);
            }
            if (fileName.ToLower().Contains("hediff")) {
                List < string > pathList;
                if (!pathsForLaodingData.TryGetValue("hediff", out pathList)) {
                    pathList = new List<string>();
                    pathsForLaodingData["hediff"] = pathList;
                }
                pathList.Add(xmlFile);
            }
            if (fileName.ToLower().Contains("backstor")) //y, ies
            {
                List < string > pathList;
                if (!pathsForLaodingData.TryGetValue("backstory", out pathList)) {
                    pathList = new List<string>();
                    pathsForLaodingData["backstory"] = pathList;
                }
                pathList.Add(xmlFile);
            }

            if (fileName.ToLower().Contains("worktype")) {
                List < string > pathList;
                if (!pathsForLaodingData.TryGetValue("worktype", out pathList)) {
                    pathList = new List<string>();
                    pathsForLaodingData["worktype"] = pathList;
                }
                pathList.Add(xmlFile);
            }
            if (fileName.ToLower().Contains("hair") || fileName.ToLower().Contains("antennae")) {
                List < string > pathList;
                if (!pathsForLaodingData.TryGetValue("hair", out pathList)) {
                    pathList = new List<string>();
                    pathsForLaodingData["hair"] = pathList;
                }
                pathList.Add(xmlFile);
            }

            if (fileName.ToLower().Contains("pawnrelations")) {
                List < string > pathList;
                if (!pathsForLaodingData.TryGetValue("pawnrelations", out pathList)) {
                    pathList = new List<string>();
                    pathsForLaodingData["pawnrelations"] = pathList;
                }
                pathList.Add(xmlFile);
            }
        }

        List < string > filePaths;
        if (pathsForLaodingData.TryGetValue("worktype", out filePaths)) {
            foreach(var filePath in filePaths)
            {
                using(var fileStream = File.OpenRead(filePath))
                    {
                        try
                        {
                        var docRoot = XDocument.Load(fileStream).Root;
                var workTypeDefsRoot = docRoot.XPathSelectElements("WorkTypeDef/workTags/..");

                if (workTypeDefsRoot.Count() == 0) {
                    fileStream.Close();
                    fileStream.Dispose();
                    continue;
                }

                var workTypeDefs = from workTypeDef in workTypeDefsRoot
                select new WorkType
                {
                    DefName = workTypeDef.Element("defName").GetValue(),
                        FullName = workTypeDef.Element("gerundLabel").GetValue(),
                        WorkTags = workTypeDef.Element("workTags")
                            .Elements("li")
                            .Select(element => element.GetValue()).ToArray()
                };

                WorkTypes.AddRange(workTypeDefs);
            }
                        catch (Exception e) {
                // Dont care
            }
            fileStream.Close();
            fileStream.Dispose();
        }
    }
}


if (pathsForLaodingData.TryGetValue("race", out filePaths)) {
    foreach(var filePath in filePaths)
    {
        using(var fileStream = File.OpenRead(filePath))
            {
                        try
                        {
                var docRoot = XDocument.Load(fileStream).Root;
        foreach(var raceVars in docRoot.Descendants("AlienRace.ThingDef_AlienRace"))
        {
            Race race = new Race();
            race.DefName = raceVars.Element("defName").GetValue();
            race.Label = raceVars.Element("label").GetValue();
            var alienraceElement = raceVars.Element("alienrace");
            if (alienraceElement == null) {
                alienraceElement = raceVars.Element("alienRace");
            }
            race.HairsByGender["Female"] = new List<Hair>();
            race.HairsByGender["Male"] = new List<Hair>();
            foreach(var bodyType in alienraceElement.XPathSelectElements("generalSettings/alienPartGenerator/alienbodytypes/li"))
            {
                race.BodyType.Add(bodyType.GetValue());
            }
            foreach(var crownType in alienraceElement.XPathSelectElements("generalSettings/alienPartGenerator/aliencrowntypes/li"))
            {
                string[] crownStrings = crownType.GetValue().Split('_');
                race.HeadType.Add(new CrownType
                                    {
                        CrownFirstType = crownStrings[0],
                        CrownSubType = crownStrings[1]
                    });
            }
            if (race.HeadType.Count == 0) {
                race.HeadType.Add(new CrownType
                                    {
                        CrownFirstType = "Average",
                        CrownSubType = "Normal"
                    });
            }
            var useGenderedHeads = alienraceElement.XPathSelectElement("generalSettings/alienPartGenerator/UseGenderedHeads");
            if (useGenderedHeads != null) {
                race.UseGenderedHeads = Convert.ToBoolean(useGenderedHeads.GetValue());
            }
            foreach(var path in alienraceElement.XPathSelectElement("graphicPaths/li").Elements())
            {
                race.GraphicPaths[path.Name.ToString().ToLower()] = path.GetValue();
            }
            RaceDictionary[race.DefName] = race;
        }

    }
                        catch (Exception e) {
        Logger.Err(e.Message);
        // Dont care
    }
    fileStream.Close();
}
                }
            }



if (pathsForLaodingData.TryGetValue("hair", out filePaths)) {
    Dictionary < string, Race > tempRaceDic = RaceDictionary.Values.ToDictionary(x => x.Label.ToLower(), x => x);
    foreach(var filePath in filePaths)
    {
        using(var fileStream = File.OpenRead(filePath))
            {
                        try
                        {
                var docRoot = XDocument.Load(fileStream).Root;
        foreach(var hairVars in docRoot.Descendants("HairDef"))
        {

            Hair hair = new Hair(hairVars.Element("hairGender").GetValue(), hairVars.Element("label").GetValue(), hairVars.Element("defName").GetValue());

            List < Race > races = new List<Race>();
            foreach(var hairTags in hairVars.XPathSelectElements("hairTags/li"))
            {
                Race race;
                if (tempRaceDic.TryGetValue(hairTags.GetValue().ToLower(), out race)) {
                    races.Add(race);
                }
            }
            if (races.Count == 0) {
                races.Add(RaceDictionary["Human"]);
            }

            foreach(var race in races)
            {
                if (hair.Gender.Equals("Any") || hair.Gender.Contains("Usually")) {
                    foreach(var list in race.HairsByGender.Values.ToList())
                    {
                        list.Add(hair);
                    }
                }
                else {
                    List < Hair > hairListForGender;
                    if (race.HairsByGender.TryGetValue(hair.Gender, out hairListForGender)) {
                        hairListForGender.Add(hair);
                    }
                }
            }
        }
    }
                        catch (Exception e) {
        Logger.Err(e.Message);
    }
    fileStream.Close();
}
                }
foreach(var race in RaceDictionary.Values.ToList())
{
    foreach(var list in race.HairsByGender.Values.ToList())
    {
        list.Sort(delegate(Hair x, Hair y)
                        {
                return x.Def.CompareTo(y.Def);
            });
    }
}
            }




if (pathsForLaodingData.TryGetValue("trait", out filePaths)) {
    foreach(var filePath in filePaths)
    {
        using(var fileStream = File.OpenRead(filePath))
            {
                        try
                        {
                var docRoot = XDocument.Load(fileStream).Root;
        foreach(var traitDef in docRoot.Descendants("TraitDef"))
        {
            var traits = (from trait in traitDef.XPathSelectElements("degreeDatas/li")
            select new TraitDef
            {
                Def = traitDef.Element("defName").Value,
                    Label = textInfo.ToTitleCase(trait.Element("label").Value),
                    Degree = trait.Element("degree") != null ? trait.Element("degree").Value : "0"
            });

            foreach(var trait in traits)
            if (!Traits.ContainsKey(trait.Def + trait.Degree))
                Traits.Add(trait.Def + trait.Degree, trait);
        }
    }
                        catch (Exception e) {
        // Dont care
    }
    fileStream.Close();

}
                }
            }


if (pathsForLaodingData.TryGetValue("hediff", out filePaths)) {
    foreach(var filePath in filePaths)
    {
        using(var fileStream = File.OpenRead(filePath))
            {
                        try
                        {
                var docRoot = XDocument.Load(fileStream).Root;

        var hediffRoots = docRoot.XPathSelectElements("HediffDef/hediffClass/..");

        if (hediffRoots.Count() == 0) {
            fileStream.Close();
            fileStream.Dispose();
            continue;
        }

        foreach(var hediffRoot in hediffRoots)
        {

            var parentClass = hediffRoot.Element("hediffClass").Value;
            var parentName = hediffRoot.Attribute("Name") != null ? hediffRoot.Attribute("Name").Value : "None";

            Hediff coreHediff;

            if (!Hediffs.TryGetValue(parentClass, out coreHediff))
                Hediffs.Add(parentClass, coreHediff = new Hediff(parentClass, parentName));

            var hediffs = (from hediff in docRoot.XPathSelectElements("//HediffDef[boolean(@ParentName) and not(@Abstract)]")
                .Where(x => x.Attribute("ParentName").Value == parentName)
            select new HediffDef
            {
                ParentClass = parentClass,
                    ParentName = hediff.Attribute("ParentName").Value,
                    Def = hediff.Element("defName").Value,
                    Label = textInfo.ToTitleCase(hediff.Element("label").Value),
                                               });

            foreach(var hediff in hediffs)
            coreHediff.SubDiffs[hediff.Def] = hediff;
        }
    }
                        catch (Exception e) {
        // Dont care
    }
    fileStream.Close();
    fileStream.Dispose();

}
                }
            }

if (pathsForLaodingData.TryGetValue("backstory", out filePaths)) {
    foreach(var filePath in filePaths)
    {
        using(var fileStream = File.OpenRead(filePath))
            {
                        try
                        {
                var docRoot = XDocument.Load(fileStream).Root;
        foreach(var def in docRoot.Descendants("AlienRace.BackstoryDef"))
        {
            Backstory backstory = new Backstory
            {
                Id = (string)def.Element("defName"),
                    Title = (string)def.Element("title"),
                        DisplayTitle = "(AlienRace)" + (string)def.Element("title"),
                            TitleShort = (string)def.Element("titleShort"),
                                Description = (string)def.Element("baseDescription"),
                                    Slot = (string)def.Element("slot"),
                                        SkillGains = new Dictionary<string, int>(),
                                        WorkDisables = new List<string>()
            };
            foreach(var skillGain in def.XPathSelectElements("skillGains/li"))
            {
                string defName = (string)skillGain.Element("defName");
                int amount = Convert.ToInt32(skillGain.Element("amount").GetValue());
                backstory.SkillGains.Add(defName, amount);
            }
            foreach(var workDisables in def.XPathSelectElements("workDisables/li"))
            {
                backstory.WorkDisables.Add(workDisables.GetValue());
            }
            ResourceLoader.Backstories[backstory.Id] = backstory;

            if (string.IsNullOrEmpty(backstory.Slot)) {
                ResourceLoader.ChildhoodStories.Add(backstory);
                ResourceLoader.AdulthoodStories.Add(backstory);
            }
            else if (backstory.Slot == "Childhood") {
                ResourceLoader.ChildhoodStories.Add(backstory);
            }
            else {
                ResourceLoader.AdulthoodStories.Add(backstory);
            }
        }
    }
                        catch (Exception e) {
        Logger.Err(e.Message);
        // Dont care
    }
    fileStream.Close();
    fileStream.Dispose();
}
                }
            }

if (pathsForLaodingData.TryGetValue("pawnrelations", out filePaths)) {
    foreach(var filePath in filePaths)
    {
        using(var fileStream = File.OpenRead(filePath))
            {
                        try
                        {
                var docRoot = XDocument.Load(fileStream).Root;
        foreach(var relationDef in docRoot.XPathSelectElements("PawnRelationDef"))
        {
            PawnRelationDefs.Add(new PawnRelationDef(relationDef));
        }
    }
                        catch (Exception e) {
        Logger.Err(e.Message);
        // Dont care
    }
    fileStream.Close();
    fileStream.Dispose();
}
                }
            }


PawnRelationDefs = PawnRelationDefs.OrderBy(x => x.DefName).ToList();
ResourceLoader.ChildhoodStories = ResourceLoader.ChildhoodStories.OrderBy(x => x.DisplayTitle).ToList();
ResourceLoader.AdulthoodStories = ResourceLoader.AdulthoodStories.OrderBy(x => x.DisplayTitle).ToList();

        }

        public bool LoadData(string path, TabControl tabControl)
{
    tabControl.TabPages.Clear();
    Animals.Clear();

    //try {
    SaveDocument = XDocument.Load(path);

    CurrentGameTick = long.Parse(SaveDocument.Root.XPathSelectElement("game/tickManager/ticksGame").GetValue());

    var playerFactionDef = EvaluateSingle<XElement>("scenario/playerFaction/factionDef").Value;

    foreach(var element in SaveDocument.Root.XPathSelectElements("game/world/factionManager/allFactions/li"))
    {
        Faction faction = new Faction(element);
        if (faction.Def.Equals(playerFactionDef)) {
            PlayerFaction = faction;
        }
        Factions[faction.FactionIDString] = faction;
        PawnsByFactions[faction] = new List<Pawn>();
    }

    //Console.WriteLine($"playerFaction:{playerFaction}, colonyFaction:{colonyFaction}");

    Dictionary < String, List < PawnData >> pawnDataDir = new Dictionary<string, List<PawnData>>();
    foreach(var pawnData in SaveDocument.Descendants("pawn"))
    {
        String key = pawnData.GetValue();

        List < PawnData > pawnDataList;
        if (!pawnDataDir.TryGetValue(key, out pawnDataList)) {
            pawnDataList = new List<PawnData>();
            pawnDataDir[key] = pawnDataList;
        }

        pawnDataList.Add(new PawnData(pawnData.Parent));
    }

    foreach(var pawn in SaveDocument.Root.XPathSelectElements("game/world/worldPawns/pawnsAlive/li"))
    {
        Pawn p = new Pawn(pawn);
        if (p.Faction != null) {
            List < PawnData > pawnDataList;
            if (!pawnDataDir.TryGetValue(p.PawnId, out pawnDataList)) {
                pawnDataList = new List<PawnData>();
            }
            p.addPawnData(pawnDataList);

            PawnsById[p.PawnId] = p;
            Faction faction = Factions[p.Faction];
            PawnsByFactions[faction].Add(p);
        }

    }

    foreach(var pawn in SaveDocument.Descendants("thing"))
    {
        if ((string)pawn.Attribute("Class") == "Pawn")
        {


            Pawn p = new Pawn(pawn);
            if (p.Faction != null) {
                List < PawnData > pawnDataList;
                if (!pawnDataDir.TryGetValue(p.PawnId, out pawnDataList)) {
                    pawnDataList = new List<PawnData>();
                }
                p.addPawnData(pawnDataList);

                PawnsById[p.PawnId] = p;
                Faction faction = Factions[p.Faction];
                PawnsByFactions[faction].Add(p);
            }

        }
    }

    if (PawnsByFactions[PlayerFaction].Count == 0) {
        throw new Exception("No characters found!\nTry playing the game a little more.");
    }

    var colonistPage = new ColonistPage(PawnsByFactions[PlayerFaction].Where(p => p.Race != null).ToList());
    colonistPage.Dock = DockStyle.Fill;
    var animalPage = new AnimalPage(PawnsByFactions[PlayerFaction].Where(p => p.Race == null).ToList());
    animalPage.Dock = DockStyle.Fill;

    TabPage colonisTabPage = new TabPage("Colonists");
    TabPage animalsTabPage = new TabPage("Animals");
    TabPage relationsTabPage = new TabPage("Relations");
    colonisTabPage.Controls.Add(colonistPage);
    animalsTabPage.Controls.Add(animalPage);
    relationsTabPage.Controls.Add(new RelationPage());


    tabControl.TabPages.Add(colonisTabPage);
    tabControl.TabPages.Add(animalsTabPage);
    tabControl.TabPages.Add(relationsTabPage);


    return true;
}

        public bool SaveData(string path)
{
    SaveDocument.Save(path);

    MessageBox.Show("Successfully saved changes!");
    return true;
}

        private IEnumerable < T > EvaluateList<T>(string eval)
{
    return (SaveDocument.Root.Element("game").XPathEvaluate(eval) as IEnumerable).Cast<T>();
}

        private T EvaluateSingle<T>(string eval)
{
    return (SaveDocument.Root.Element("game").XPathEvaluate(eval) as IEnumerable).Cast<T>().First();
}

        private T Evaluate<T>(string eval)
{
    return (T)SaveDocument.Root.Element("game").XPathEvaluate(eval);
}

        private IEnumerable < T > EvaluateList<T>(XNode node, string eval)
{
    return (node.XPathEvaluate(eval) as IEnumerable).Cast<T>();
}

        private T EvaluateSingle<T>(XNode node, string eval)
{
    return (node.XPathEvaluate(eval) as IEnumerable).Cast<T>().First();
}

        private T Evaluate<T>(XNode node, string eval)
{
    return (T)node.XPathEvaluate(eval);
}


}
