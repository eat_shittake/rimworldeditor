import axios, { AxiosResponse } from "axios";
import Routes from "./Routes";
import {Logger} from "./Logger"

export class Api {

    private GetBackstories(): Promise<string> {
        return axios.get(Routes.BackStoriesRoute)
            .then((response: AxiosResponse<string>) => {
                return response.data;
        }).catch(()=>{
            Logger.Err("Unable to get backstories!");
            return "";
        });
    }

    private GetBackStoryTranslations(): Promise<string> {
        return axios.get(Routes.BackStoryTranslationsRoute)
            .then((response: AxiosResponse<string>) => {
                return response.data;
        }).catch(()=>{
            Logger.Err("Unable to get translations!");
            return "";
        });
    }

    private GetBodyparts(): Promise<string> {
        return axios.get(Routes.BodyPartsRoute)
            .then((response: AxiosResponse<string>) => {
                return response.data;
        }).catch(()=>{
            Logger.Err("Unable to get bodyparts!");
            return "";
        });
    }

    private GetPawnBios(): Promise<string> {
        return axios.get(Routes.PawnBiosRoute)
            .then((response: AxiosResponse<string>) => {
                return response.data;
        }).catch(()=>{
            Logger.Err("Unable to get bios!");
            return "";
        });
    }

    public GetAll(): Promise<Array<string>>{
        return axios.all([this.GetBodyparts(),this.GetBackStoryTranslations(),this.GetBackstories(), this.GetPawnBios()])
            .then(axios.spread((bodyparts, translations, backstories, bios) =>{
                return [bodyparts, translations, backstories, bios];
        }));
    }
}

export var ApiService = new Api();