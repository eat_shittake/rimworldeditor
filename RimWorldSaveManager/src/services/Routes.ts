export default class Routes{
    public static BackStoriesRoute = "../resources/BackStories.xml";
    public static BackStoryTranslationsRoute = "../resources/BackstoryTranslations.xml";
    public static BodyPartsRoute = "../resources/Bodyparts.xml";
    public static PawnBiosRoute = "../resources/PawnBios.xml";
}