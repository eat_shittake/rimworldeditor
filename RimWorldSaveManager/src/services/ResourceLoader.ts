﻿import Backstory from "../models/Backstory";
import { ApiService } from "./ApiService";
import { modelFactory } from "./ModelFactory";
import { Logger } from "./Logger";
import * as cheerio from "cheerio";
import Bodyparts from "../models/Bodyparts";
import BackstoryTranslation from "../models/BackstoryTranslation";
import { BackstoryType } from "../enums/enums";
import * as _ from "lodash";

export class ResourceLoader {

    public static Backstories: Array<Backstory>;
    public static BackstoryTranslations: Array<BackstoryTranslation>;
    public static ChildhoodStories: Array<Backstory>;
    public static AdulthoodStories: Array<Backstory>;
    public static Bodyparts: Bodyparts;

    constructor() {
        ResourceLoader.Backstories = [];
        ResourceLoader.ChildhoodStories = [];
        ResourceLoader.AdulthoodStories = [];
        ResourceLoader.Bodyparts = new Bodyparts();
    }

    public Load(): void {
        Logger.Debug("Loading backstory database.");
        let none = new Backstory();
        none.Id, none.Description = "None";
        none.Title = "-- No Backstory --";
        none.Slot = "Both"
        ResourceLoader.Backstories.push(none)
        ResourceLoader.AdulthoodStories.push(none);
        ResourceLoader.ChildhoodStories.push(none);

        ApiService.GetAll().then(response => {
            //Begin bodyparts
            let bodyparts = response.shift();
            let elements = cheerio.load(bodyparts, {
                normalizeWhitespace: true,
                xmlMode: true
            })("Bodyparts").children().each((i, elem) => {
                ResourceLoader.Bodyparts.SetBodyPart(Number(elem.firstChild.nodeValue), elem.lastChild.nodeValue)
            });
            //End bodyparts
            //Start translations
            let translations = response.shift();
            ResourceLoader.BackstoryTranslations = modelFactory.ExtractBackstoryTranslations(cheerio.load(translations, {
                normalizeWhitespace: true,
                xmlMode: true
            })("BackstoryTranslations").children().toArray());
            //End Translations
            //Start backstories
            let backstories = response.shift();

            let backstoryModels = modelFactory.ExtractBackStories(cheerio.load(backstories, {
                normalizeWhitespace: true,
                xmlMode: true
            })("Backstories"));

            backstoryModels.forEach(model => {
                if (_.toLower(model.Slot) == _.toLower(BackstoryType.ADULTHOOD)) {
                    ResourceLoader.AdulthoodStories.push(model);
                } else if (_.toLower(model.Slot) == _.toLower(BackstoryType.CHILDHOOD)) {
                    ResourceLoader.ChildhoodStories.push(model);
                }
                ResourceLoader.Backstories.push(model);
            });
            //End Backstories
            //Start bios
            let bios = response.shift();
            let base = cheerio.load(bios, { normalizeWhitespace: true, xmlMode: true })("PlayerCreatedBios");
            let childhood = base.find(BackstoryType.CHILDHOOD);
            let childhoodModels = modelFactory.ExtractBackstoryFromBio(BackstoryType.CHILDHOOD, childhood);
            ResourceLoader.Backstories = childhoodModels.concat(ResourceLoader.Backstories);
            ResourceLoader.ChildhoodStories = childhoodModels.concat(ResourceLoader.ChildhoodStories);
            let adulthood = base.find(BackstoryType.ADULTHOOD);
            let adulthoodModels = modelFactory.ExtractBackstoryFromBio(BackstoryType.ADULTHOOD, adulthood);
            ResourceLoader.Backstories = adulthoodModels.concat(ResourceLoader.Backstories);
            ResourceLoader.AdulthoodStories = adulthoodModels.concat(ResourceLoader.AdulthoodStories);
            //End bios
        }).catch(() => {
            Logger.Err("ResourceLoader unable to load!");
        });

    }
}

export var resourceLoader = new ResourceLoader();
