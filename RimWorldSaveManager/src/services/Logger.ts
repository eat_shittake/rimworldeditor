﻿export class LoggerService {
    
    public Debug(msg: string) {
        console.debug(`[DEBUG] ${msg}`);
    }

    public Warn(msg: string) {
        console.warn(`[WARN] ${msg}`);
    }

    public Err(msg) {
        console.error(`[ERROR] ${msg}`);
    }
}

export var Logger = new LoggerService();