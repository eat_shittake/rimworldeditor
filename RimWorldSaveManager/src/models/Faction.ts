﻿export default class Faction {

    constructor(
        public loadID: string,
        public name: string,
        public def: string) { }

    public get FactionIDString(): string {
        return `Faction_${this.loadID}`;
    }


    public ToString(): string {
        if (!this.name.length) {
            return this.FactionIDString;
        }
        return this.name;
    }
}
