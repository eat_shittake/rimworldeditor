﻿import AbstractName from "./AbstractName";

export default class HumanoidName extends AbstractName {

    constructor(
        Class: string,
        public first: string,
        public nick: string,
        public last: string) {
        super(Class);
    }

    public FullName(): string {
        return `${this.first} ${this.nick == this.last || this.nick == this.first ? " " : (` "${this.nick}" `)} ${this.last}`;
    }
}