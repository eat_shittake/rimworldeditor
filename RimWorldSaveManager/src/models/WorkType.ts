﻿export default class WorkType {
    public DefName: string;
    public FullName: string;
    public WorkTags: string[];
}
