﻿export default class PawnTrait {
    constructor(public def: string,
        public degree: string,
        public label: string) {

        //TODO Put this outside constructor, maybe inside factory
        //var traitKey = def + degree;
        //this.label = DataLoader.Traits.ContainsKey(traitKey) ? DataLoader.Traits[traitKey].Label : Def;
    }

    public ToString(): string {
        return this.label;
    }

    public static Create(def: TraitDef): PawnTrait {
        return new PawnTrait(def.Def, def.Degree, def.Label);
    }


}
