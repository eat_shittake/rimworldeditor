﻿import AbstractName from "./AbstractName";

export default class AnimalName extends AbstractName {

    constructor(
        Class: string,
        public name: string,
        public numerical: string) {
        super(Class);
    }

    public FullName(): string {
        return this.name;
    }
}