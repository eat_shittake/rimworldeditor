export default class Bodyparts {
    private bodyPartDictionary: {[index: number]:string}
    constructor(){
        this.bodyPartDictionary = {}
    }

    public SetBodyPart(index: number, partName: string): void {
        this.bodyPartDictionary[index] = partName;
    }

    public GetBodyPart(index: number): string {
        if(this.bodyPartDictionary[index]){
            return this.bodyPartDictionary[index]            
        }
        return "Unknown";
    }
}