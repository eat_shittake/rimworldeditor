﻿export default class PawnHealth {
    constructor(
        public ParentClass: string,
        public def: string,
        public partIndex: string,
        public label: string,
        public pawnDef: string,
        public description: string) { //DataService.HumanBodyPartDescription[this.partIndex]
    }

    public ToString(): string {
        let description: string;
        if (this.pawnDef === "Human") {
            description = this.partIndex !== null ? this.description : null;
        }
        return (this.label || this.def) + (description != null ? (` - ${description}`) : "");
    }
}