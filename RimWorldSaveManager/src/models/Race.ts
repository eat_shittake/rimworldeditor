﻿export default class Race {
    public static HUMAN = "Human";
    public defName: string;
    public label: string;
    public useGenderedHeads: boolean = true;
    public graphicPaths: { [key: string]: string };
    public bodyType: string[];
    public headType: CrownType[];
    public hairsByGender: { [key: string]: Array<Hair> };
    constructor() {
        this.graphicPaths = {};
        this.hairsByGender = {};
    }
}
