﻿export default class PawnSkill {
    constructor(private def: string,
        public level: number,
        public xpSinceLastLevel: number,
        public passion: string) { }

    public get Name(): string {
        return this.def;
    }

    public set Name(name: string) {
        this.def = name;
    }
}
