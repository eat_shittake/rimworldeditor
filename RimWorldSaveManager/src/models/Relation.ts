﻿export default class Relation {

    constructor(public otherPawn: string,
        public def: string,
        public startTicks: number) { }

    public static Create(pawnRelationDef: PawnRelationDef): Relation {
        return new Relation("", pawnRelationDef.defName, 0);
    }

    public ToString(): string {
        return this.def;
    }
}
