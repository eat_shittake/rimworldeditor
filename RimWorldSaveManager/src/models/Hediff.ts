﻿import HediffDef from "./HediffDef";

export default class Hediff {
    public SubDiffs: { [key: string]: HediffDef };

    constructor(public parentClass: string, public parentName: string) {
        this.SubDiffs = {};
    }
}
