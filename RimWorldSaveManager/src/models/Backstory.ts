export default class Backstory {
    public SkillGains: {[name:string]: number};
    public Id: string
    public WorkDisables: Array<string>
    public Title: string;
    public TitleShort: string;
    public Description: string;
    public Slot: string;

    constructor(){
        this.SkillGains = {};
        this.WorkDisables = [];
    }
            
    
    public ToString(): string {
        return this.Title;
    }
}
    