﻿import AbstractName from "../models/AbstractName";

export default class PawnData {

    constructor(
        public name: AbstractName,
        public gender: string) { }

}
