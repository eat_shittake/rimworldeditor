﻿export default class AbstractName {

    constructor(public nameClass: string) { }

    public FullName(): string {
        throw "Not implemented. Implement in subclass.";
    }

    public ToString(): string {
        return this.FullName();
    }
}
