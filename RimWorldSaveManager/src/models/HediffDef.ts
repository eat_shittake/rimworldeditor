﻿export default class HediffDef {
    public parentClass: string;
    public def: string;
    public parentName: string;
    public label: string;
}
