﻿export default class CrownType {

    constructor(public crownFirstType: string,
    public crownSubType: string){}

    public get CombinedCrownLabel(): string {
        return `${this.crownFirstType} ${this.crownSubType}`;
    }

    public get CombinedCrownDef(): string {
        return `${this.crownFirstType}_${this.crownSubType}`;
    }

    public ToString(): string {
        return this.CombinedCrownLabel;
    }
}
