﻿export default class Hair {

    constructor(
        public gender: string,
        public title: string,
        public def: string) { }

    public ToString(): string {
        return this.def;
    }
}
