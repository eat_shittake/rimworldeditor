﻿export default class Training {

    constructor(
        public pawnDef: string,
        public ObedienceTraining: boolean,
        public ReleaseTraining: boolean,
        public RescueTraining: boolean,
        public HaulTraining: boolean,
        public ObedienceStep: number,
        public ReleaseStep: number,
        public RescueStep: number,
        public HaulStep: number) {
    }
}
