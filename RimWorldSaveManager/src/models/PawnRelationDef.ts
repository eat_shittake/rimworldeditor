﻿export default class PawnRelationDef {

    constructor(public defName: string,
        public label: string) { }

    public ToString(): string {
        return this.defName;
    }
}

