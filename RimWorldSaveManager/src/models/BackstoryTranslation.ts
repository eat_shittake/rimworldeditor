export default class BackstoryTranslation {
    public TagName: string;
    public Title: string;
    public TitleShort: string;
    public BaseDesc: string;
}