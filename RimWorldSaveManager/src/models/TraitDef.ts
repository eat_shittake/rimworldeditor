﻿export default class TraitDef {

    public Def: string;
    public Degree: string;
    public Label: string;

    public ToString(): string {
        return this.Label;
    }
}
