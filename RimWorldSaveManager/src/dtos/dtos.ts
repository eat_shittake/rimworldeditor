﻿class SaveFileDto {
    meta: [MetaDto];
    game: [{/** TODO **/ }];
}

class MetaDto {
    gameVersion: [TString];
    modIds: [Li<TString>];
    modNames: [Li<TString>];
}

class PawnDto {
    def: [TString];
    id: [TString];
    pos: [TString];
    faction: [TString];
    boughtItems: [Li<TString>];
    guestArea: [TString];
    shoppingArea: [TString];
    drugPolicy: [NullCheck];
    kindDef: [TString];
    name: [HumanoidNameDto | AnimalNameDto];
    mindState: [MindstateDto];
    jobs: [NullCheck];
    stances: [NullCheck];
    verbTracker: [{ verbs: [Li<VerbDto>] }];
    natives: [NullCheck];
    meleeVerbs: [MeleeVerbsDto];
    rotationTracker: [NullCheck];
    pather: [NullCheck];
    carryTracker: [{ innerContainer: [InnerContainerDto<any>] }]; //TODO Not sure what this is/could be, so we'll hold off.
    apparel: [ApparelDto];
    story: [StoryDto];
    inventory: [InventoryDto];
    filth: [NullCheck];
    needs: [{ needs: [Li<NeedsDto>] }];
    guest: [GuestDto];
    guilt: [{ lastGuiltyTick: [TNumber] }];
    social: [SocialDto];
    ownership: [{ ownedBed: [TString], assignedGrave: [TString] }];
    interactions: [NullCheck];
    skills: [SkillsDto];
    workSettings: [{ priorities: [NullCheck] }];
    trader: [NullCheck];
    outfits: [NullCheck];
    drugs: [NullCheck];
    timetable: [NullCheck];
    playerSettings: [PlayerSettingsDto];
    training: [NullCheck];
}

class MindstateDto {
    meleeThreat: [TString];
    enemyTarget: [TString];
    knownExploder: [TString];
    lastMannedThing: [TString];
    thinkData: [KeyValue<TNumber, TNumber>];
    canFleeIndividual: [TBool];
    duty: [NullCheck];
    mentalStateHandler: [{ curState: [NullCheck] }];
    mentalBreaker: [{ ticksBelowMinor: [TNumber] }];
    inspirationHandler: [{ curState: [NullCheck] }];
    priorityWork: [{ prioritizedCell: [TString] }];
    applyBedThoughtsOnLeave: [TBool];
}

class StoryDto {
    childhood: [TString];
    adulthood: [TString];
    bodyType: [TString];
    crownType: [TString];
    headGraphicPath: [TString];
    hairDef: [TString];
    hairColor: [TString];
    melanin: [TNumber];
    traits: [{ allTraits: [Li<TraitDto>] }];
    equipment: [{ equipment: [{ innerList: [Li<any>] }]; }];
    drafter: [NullCheck];
    ageTracker: [AgeTrackerDto];
    healthTracker: [HealthTrackerDto];
    records: [{ records: [{ vals: [Li<TNumber>] }] }];
    storyRelevance: [TNumber];
    battleActive: [TString];
}

class TraitDto {
    def: [TString];
    degree: [TNumber];
}

class HealthTrackerDto {
    hediffSet: [{ hediffs: [Li<HediffDto>] }];
    surgeryBills: [{ bills: [Li<any>] }];
    immunity: [{ imList: [Li<any>] }];
}

class AgeTrackerDto {
    ageBiologicalTicks: [TNumber];
    birthAbsTicks: [TNumber];
}

class BaseNameDto {
    Class: string;
}

class AnimalNameDto extends BaseNameDto {
    name: [TString];
    numerical: [TString];
}

class HumanoidNameDto extends BaseNameDto {
    first: [TString];
    nick: [TString];
    last: [TString];
}

class TrainingDto {
    wantedTrainables: [{
        vals: [Li<TBool>] //These arrays are only gonna be 4 elements
    }];
    steps: [{
        vals: [Li<TNumber>]
    }];
}

class InnerContainerDto<T> {
    maxStacks: [TNumber];
    innerList: [Li<T>];
}

class VerbDto {
    Class: string;
    loadID: [TString];
}

class HediffDto extends VerbDto {
    def: [TString];
    ageTicks: [TString];
    partIndex: [TNumber];
    severity: [TNumber];
    visible: [TBool];
    tendTick: [TNumber];
    tendQuality: [TNumber];
    tendedCount: [TNumber];
    lastInjury: [TString];
}

class MeleeVerbsDto {
    curMeleeVerb: [TString];
    curMeleeVerbUpdateTick: [TNumber];
}

class ApparelDto {
    wornApparel: [{ innerList: [Li<any>] }]; //TODO Not sure what this is/could be, so we'll hold off.
    lastApparelWearoutTick: [TNumber];
}

class InventoryDto {
    itemsNotForSale: [Li<any>];
    innerContainer: [{ innerList: [Li<any>] }];
}

class NeedsDto {
    Class: string;
    def: [TString];
    curLevel: [TNumber];
    thoughts: [{ memories: [{ memories: [Li<MemoryDto>] }] }]; 
    recentMemory: [RecentMemoryDto];
}

class RecentMemoryDto {
    lastLightTick: [TNumber]; 
    lastOutdoorTick: [TNumber];
}

class MemoryDto {
    Class: string;
    def: [TString];
    otherPawn: [TString];
    moodPowerFactor: [TNumber];
    age: [TNumber];
    opinionOffset: [TNumber];
}

class GuestDto {
    hostFaction: [TString];
    getsFood: [TBool];
    interactionMode: [TString];
    spotToWaitInsteadOfEscaping: [TString];
    lastPrisonBreakTicks: [TNumber];
}

class SocialDto {
    directRelations: [Li<any>]; //TODO, fix this
    relativeInvolvedInRescueQuest: [TString];
}

class SingleSkillDto {
    def: [TString];
    level: [TNumber];
    xpSinceLastLevel?: [TNumber];
    passion?: [TString]; 
    xpSinceMidnight?:[TNumber];
}

class SkillsDto {
    skills: [Li<SingleSkillDto>];
    lastXpSinceMidnightResetTimestamp: [TNumber];
}

class PlayerSettingsDto {
    joinTick: [TNumber];
    medCare: [TString];
    areaAllowed: [TString];
    master: [TString];
    followDrafted: [TBool];
    followFieldwork: [TBool];
}

class TBool {
    $t: boolean;
}

class TNumber {
    $t: number;
}

class TString {
    $t: string
}

class NullCheck {
    IsNull: boolean
}

class Li<T> {
    li: Array<T>
}

class KeyValue<K, V> {
    keys: [Li<K>];
    vals: [Li<V>];
}
