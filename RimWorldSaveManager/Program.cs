﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RimWorldSaveManager
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
		    //Console.BufferHeight = 2000;

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new MainForm());
            //https://www.npmjs.com/package/xml2json
            //var xml2json = require("xml2json")
            //var xml = `<name Class="NameTriple">
            //    <first>Green</first>
            //    <nick>Shrew</nick>
            //    <last>Shrew</last>
            //    </name>`;
            //var json = xml2json.toJson(xml, {arrayNotation: true, reversible: true});
            //console.log(json)
            //console.log(xml2json.toXml(json, {arrayNotation: true}))
        }
    }
}
