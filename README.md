# RimWorld Save Editor #
This is a SaveGame-Editor for RimWorld (rimworldgame.com).

It will eventually support editing the follwing stats:  
**Colonists**:  Name, Skills, Age, Traits, Backstory, Injuries, Gender, Bodytype, Skincolor, Headtype, Hair, Haircolor, Relations  
**Animals**: Name, Age, Injuries, Training, Relations  
Supports races made with the Humanoid Alien Framework.  

This will be made available online when it's ready.

**Thanks @ RedJordan95, DragonLord and arkatufus for their work on this project!**

#### Disclaimer: Use at your own risk, I am not responsible for your loss of progress. ####

### ChangeLog ###

#### v0.1 ####
* Converts codebase to TypeScript project.